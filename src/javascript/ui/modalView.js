import View from '../view';
import Fighter from '../model/fighter';

class ModalView extends View {

    name;
    healthInput;
    healthValue;
    closeModal;

    saveBtn;

    constructor(handler) {
        super();

        this.element = document.getElementById('id_modal');
        this.name = document.getElementById('fighterName');
        this.health = document.getElementById('fighterHealth');
        this.attack = document.getElementById('fighterAttack');
        this.defense = document.getElementById('fighterDefense');
        
        this.saveBtn = document.getElementById('id_modal_save');

        this.closeModal = document.getElementById('id_modal_close');
        this.closeModal.addEventListener('click', event => this.closeFighterDetailsModal(event), false);
        this.saveBtn.addEventListener('click', event => handler(event, this.collectValues()), false);
    }

    showModal(fighter) {
        this.fighterId = fighter.id;
        this.element.style.display = "block";
        this.name.innerText = fighter.name;
        this.health.setAttribute('value', fighter.health);
        this.health.value = fighter.health;
        this.attack.setAttribute('value', fighter.attack);
        this.defense.setAttribute('value', fighter.defense);
    }

    closeFighterDetailsModal() {
        this.element.style.display = "none";
        this.fighterId = null;
    }

    collectValues() {
        const attributes = {
            '_id' : this.fighterId,
            'name' : this.name.innerText,
            'health' : this.health.value,
            'attack' : this.attack.getAttribute('value'),
            'defense' : this.defense.getAttribute('value')
        }; 
      return new Fighter(attributes);
    }

}

export default ModalView;