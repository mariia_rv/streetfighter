import View from './view';

class FighterView extends View {

  constructor(fighter, handleImageClick, handleHitClick) {
    super();

    this.createFighter(fighter, handleImageClick, handleHitClick);
  }

  createFighter(fighter, handleImageClick, handleHitClick) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    this.imageElement = this.createImage(source);
    this.checkBoxElement = this.createCheckbox(fighter._id, name);
    const progressBarElements = this.createProgressBar();
    this.progressBarContainer = progressBarElements[0];
    this.progressBarHealth = progressBarElements[1];
    this.btnHit = this.createBtnHit();

    this.element = this.createElement({ tagName: 'div', className: 'fighter'});
    this.element.id = `fighter-container-${fighter._id}`;
  
    this.element.append(this.imageElement, nameElement, this.checkBoxElement, this.progressBarContainer, this.btnHit);

    this.btnHit.addEventListener('click', event => handleHitClick(event, fighter._id), false);
    this.imageElement.addEventListener('click', event => handleImageClick(event, fighter), false);
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }

  createCheckbox(_id, name) {
    const attributes = {
      type: 'checkbox',
      name: name,
      value: _id,
      id: `checkBox-${_id}`
    };
    const checkBoxElement = this.createElement({
      tagName: 'input', 
      className: 'checkboxFighter',
      attributes
    });
    checkBoxElement.innerText = name;
  
    return checkBoxElement;
  }

  createProgressBar(_id) {
    const healthElemContain = this.createElement({tagName: 'div', className: 'healthContainer', id: `containerHealth-${_id}`});
    const healthElem = this.createElement({tagName: 'div', className: 'healthProgress', id: `health-${_id}`});
    healthElem.innerText = ''; // todo add percent
    healthElemContain.append(healthElem);
    
    return [ healthElemContain, healthElem ];
  }

  createBtnHit(_id) {
    const attributes = {
      type: 'button',
      value: 'HIT',
      id: `btnHit-${_id}`
    }
    const btnHit = this.createElement({
      tagName: 'input',
      className: 'btnHit',
      attributes
    });

    return btnHit;
  }

  isFighterChecked() {
    return this.checkBoxElement.checked;
  }

  onFightStarted(isParticipatingInFight) {
      this.progressBarContainer.style.visibility = isParticipatingInFight ? 'visible' : 'hidden';
      this.progressBarHealth.style.width = '100%';
      this.btnHit.style.opacity = isParticipatingInFight ? '1' : '0';
      this.checkBoxElement.disabled = true;
      // this.imageElement.disabled = true; // TODO
  }

  onFightFinished(){
    this.progressBarContainer.style.visibility = 'hidden';
    this.btnHit.style.opacity = '0';
    this.checkBoxElement.disabled = false;
    this.checkBoxElement.checked = false;
    // this.imageElement.disabled = false; // TODO
  }

  updateHealth(initialHealth, newHealth) {
    console.log(`Initial health: ${initialHealth}, updated health: ${newHealth}`);
    this.progressBarHealth.style.width = `${newHealth/initialHealth*100}%`
  }
}

export default FighterView;