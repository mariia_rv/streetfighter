import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import ModalView  from './ui/modalView';
import Fighter from './model/fighter';
import { runInThisContext } from 'vm';

class FightersView extends View {

  constructor(fighters) {
    super();

    this.handleFighterImageClick = this.handleFighterClick.bind(this);
    this.handleSaveClick = this.handleSaveFighter.bind(this);
    this.handleHit = this.handleHitClick.bind(this);
    
    this.fightersDetailsMap = new Map();
    this.activeFightersArray = new Array();

    this.modalView = new ModalView(this.handleSaveClick);
    this.btnStart = this.createStartFightBtn();
    this.fighterViews = this.createFighters(fighters);  
  }

  createFighters(fighters) {
    const fighterViews = fighters.map(fighter => {
      return new FighterView(fighter, this.handleFighterImageClick, this.handleHit);
    });
    const fighterElements = fighterViews.map(fighterView => {
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
    return fighterViews;
  }

  createStartFightBtn(){
    const attributes = {
      type: 'button',
      value: 'Fight',
    }
    const rootElement = document.getElementById('root');
    const buttonStart = this.createElement({ tagName: 'input', className: 'btnStart', attributes});
    buttonStart.addEventListener('click', event => this.handleStartFightClick(event), false );
    rootElement.append(buttonStart);

    return buttonStart;
  }

  handleFighterClick(event, fighter) {
    this.getFighterDetails(fighter._id).then(details => this.showFighterDetailsModal(details));
  }

  handleSaveFighter(event, fighter) {
    this.modalView.closeFighterDetailsModal();
    this.fightersDetailsMap.set(fighter.id, fighter);
    console.log("Saving fighter: " + JSON.stringify(fighter));
  }

  showFighterDetailsModal(fighterDetails) {
    this.modalView.showModal(fighterDetails);
  }

  handleStartFightClick(event) {
    let selectedFighterIds = Array.from(document.getElementsByClassName('checkboxFighter'))
      .filter(function(input) {
        return input.checked;
      })
      .map(input => {
        return input.value;
      });

    if (selectedFighterIds.length < 2) {
      alert("There was fight between two invisible fighters. You missed it! In order to see real fight select exact 2 fighters!");
    } else if (selectedFighterIds.length > 2) {
      alert("This game is not about medieval war. Please choose exactly 2 fighters.");
    } else {
      let notAvailableInCache = selectedFighterIds.filter(id => !this.fightersDetailsMap.has(id));
      if (notAvailableInCache.length > 0) {
        let detailsPromises = [];
        selectedFighterIds.forEach(id => {
          if (!this.fightersDetailsMap.has(id)) {
            detailsPromises.push(this.getFighterDetails(id));
          }
        });
  
        Promise.all(detailsPromises).then(result => {
          this.activeFightersArray = result.map(f => new Fighter(f));
          this.onFightStarted();
          console.log(this.activeFightersArray);
        });
      } else {
        this.activeFightersArray = Array.from(this.fightersDetailsMap.values())
          .filter(fighter => selectedFighterIds.includes(fighter.id))
          .map(f => new Fighter(f));
        this.onFightStarted();
      }
    }
  }

  onFightStarted() {
    if (this.activeFightersArray.length != 2) {
      console.log("Error: active fighters count is not equal 2!")
    }
    this.btnStart.style.opacity = '0';
    this.fighterViews
      .forEach(view => view.onFightStarted(view.isFighterChecked()));
  }

  onFightFinished(winnerName) {
    this.btnStart.style.opacity = '1';
    this.activeFightersArray = [];
    this.fighterViews.forEach(view => view.onFightFinished());
    alert(`Fight finished! ${winnerName} winned!`);
  }
  
  handleHitClick(event, id) {
    console.log(event, id);
    let hittingFighter = this.activeFightersArray.filter(f => f.id == id).pop();
    let hittedFighter = this.activeFightersArray.filter(f => f.id != id).pop();

    let damage = hittingFighter.getHitPower() - hittedFighter.getBlockPower();
    damage = damage > 0 ? damage : 0;
    console.log(`${hittingFighter.name} is hitting with damage=${damage}`);
    hittedFighter.health = hittedFighter.health - damage;
    
    if (hittedFighter.health <= 0) {
      this.onFightFinished(hittingFighter.name);
    } else {
      const initialHealth = this.fightersDetailsMap.get(hittedFighter.id).health;
      const hittedFighterView = this.fighterViews
        .filter(v => v.element.id.endsWith(hittedFighter.id))
        .pop();
      hittedFighterView.updateHealth(initialHealth, hittedFighter.health);
    }
  }

  async getFighterDetails(id) {
      if (!this.fightersDetailsMap.has(id)){
        const fighterDetails = await fighterService.getFighterDetails(id);
        console.log("Got fighter details: " + JSON.stringify(fighterDetails));
        this.fightersDetailsMap.set(id, fighterDetails);
      }
      return this.fightersDetailsMap.get(id);
    }

}

export default FightersView;