class Fighter {

    MULTIPLIER_MIN = 1;
    MULTIPLIER_MAX = 2;

    static fromFighter(fighter) {
        let newFighter = new Fighter(fighter);
        newFighter.id = fighter.id;
        return newFighter;        
    }

    constructor(attributes){
        if (attributes._id != undefined) {
            this.id = attributes._id;    
        } else {
            this.id = attributes.id;
        }
        this.name = attributes.name;
        this.health = attributes.health;
        this.attack = attributes.attack;
        this.defense = attributes.defense;
        this.validate();
    }

    validate() {
        for(let i in this){
            if(i == undefined || i == null){
                throw new Error('Provide all Fighter attributes');
            }
        }
    }

    getHitPower() {
        const criticalHitChance = this.getMultiplier(); 
        return this.attack * criticalHitChance;
    }

    getBlockPower() {
        const dodgeChance = this.getMultiplier();
        return this.defense * dodgeChance;
    }

    getMultiplier() {
        return Math.floor(Math.random() * (this.MULTIPLIER_MAX - this.MULTIPLIER_MIN + 1)) + this.MULTIPLIER_MIN;
    }

}

export default Fighter;